'use strict'

const formatHelper = require('./formatHelper')

const coinParser = (() => {
	const parseTotalMarketCap = totalMarketCap => {
		return formatHelper.formatAmount(totalMarketCap.data.quote.USD.total_market_cap)
	}

	const parseCoins = coins => {
		const parsedCoins = Object.keys(coins.data).map(coin => (
			{
				'name': coins.data[coin].name,
				'rank': coins.data[coin].cmc_rank,
				'price': formatHelper.formatAmount(coins.data[coin].quote.USD.price),
				'percent_change_1h': formatHelper.formatPercentage(coins.data[coin].quote.USD.percent_change_1h),
				'percent_change_24h': formatHelper.formatPercentage(coins.data[coin].quote.USD.percent_change_24h),
				'percent_change_7d': formatHelper.formatPercentage(coins.data[coin].quote.USD.percent_change_7d),
				'market_cap': formatHelper.formatAmount(coins.data[coin].quote.USD.market_cap)
			}
		))
		return parsedCoins.sort((a, b) => (a.rank - b.rank))
	}

	return {
		parseTotalMarketCap,
		parseCoins
	}
})()

module.exports = coinParser
