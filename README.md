# Tiny Coin Cap

[![Website](https://img.shields.io/website?url=https%3A%2F%2Ftinycoincap.com)](https://tinycoincap.com)
[![Hippocratic License HL3-FULL](https://img.shields.io/static/v1?label=Hippocratic%20License&message=HL3-FULL&labelColor=5e2751&color=bc8c3d)](https://firstdonoharm.dev/version/3/0/full.html)
[![FOSSA Status](https://app.fossa.com/api/projects/git%2Bgitlab.com%2Fnitrohorse%2Ftiny-coin-cap.svg?type=shield)](https://app.fossa.com/projects/git%2Bgitlab.com%2Fnitrohorse%2Ftiny-coin-cap?ref=badge_shield)

https://tinycoincap.com

Minimal [CoinMarketCap.com](https://coinmarketcap.com/) clone rendered server-side so visitors who value their privacy can leave JavaScript disabled with no impact to site functionality. Powered by [CoinMarketCap.com](https://coinmarketcap.com/api/) and [Vercel](https://vercel.com/). Inspired by [LegibleNews.com](https://legiblenews.com/) and [LiteMarketCap.com](https://litemarketcap.com/).

## Develop Locally
* Clone the repo
* Install tools:
	* [Node.js](https://nodejs.org)
	* [nvm](https://github.com/nvm-sh/nvm)
* Use specified Node version:
	* `nvm use`
* Install dependencies:
	* `npm i`
* Start the server:
  * `npm start`
